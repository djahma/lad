package main

import (
	"C"
)
import (
	"fmt"
	"os"

	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/gui"
	"github.com/therecipe/qt/qml"
	"github.com/therecipe/qt/quickcontrols2"
)

func main() {
	// TODO: read conf file (incl ladui)
	// TODO: look for current application install directory
	// TODO: spot lad.conf.json file
	// TODO: if not found create it with default values
	// TODO: load file into settings struct

	// TODO: Launch lad_sys RPC
	// TODO: Launch lad_ui
	// TODO: Launch lad_ui RPC
	// TODO: discover plugins
	// TODO: order plugins
	// TODO: Launch plugins
	fmt.Println("print some")
	// Create application
	app := gui.NewQGuiApplication(len(os.Args), os.Args)

	// Enable high DPI scaling
	app.SetAttribute(core.Qt__AA_EnableHighDpiScaling, true)

	// Use the material style for qml
	quickcontrols2.QQuickStyle_SetStyle("Fusion")

	// Create a QML application engine
	engine := qml.NewQQmlApplicationEngine(nil)

	// Load the main qml file
	engine.Load(core.NewQUrl3("qrc:/qml/main.qml", 0))

	// Execute app
	gui.QGuiApplication_Exec()
}
