[Work in Progess]

# Concept
LAD is a multi-pane GUI desktop app written in Go and QML, whose objective is to provide a highly flexible, plugin-based, data analysis tool.
Plugins are any gRPC binaries implementing the `plugin` API.

# Installation
Nothing to install yet.
